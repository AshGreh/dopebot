const commando = require('discord.js-commando');
const settings = require('./ignore/settings.json');
var filter = require('./ignore/filter.json')
const json = require("jsonfile")
const delay = require('delay')
require('import-export');


const bot = new commando.Client({
    owner: settings.owner,
    commandPrefix: settings.prefix,
    unknownCommandResponse: false
})

function refreshFilter() {
    filter = json.readFileSync("./ignore/filter.json")
    console.log("Filtering the words:")
    for(word of filter["words"])
        console.log(word)
}

function mute(message, message, code) {
    let toMute = message.author
    message.channel.send(toMute+" has been muted for saying words that are not allowed. You may appeal the mute at https://www.powerbot.org/community/forum/381-support/. ")
    toMute.send("You have been muted for saying words that are not allowed by the Filter System. You may appeal the mute at https://www.powerbot.org/community/forum/381-support/. ")
    message.guild.owner.send(`${toMute} was muted for saying "${code}".`)
}

bot.on("ready", async () => {
    console.log(`${bot.user.username} has started!\nShare this link:`);
    try {
        let link = await bot.generateInvite("2121493711");
        console.log(link);
    } catch(e) {
        console.log(e.stack)
    }
    refreshFilter()
});

bot.on("message", async message => {
    var msg = message.content.toLocaleLowerCase()
    if(message.author.bot)
        return
    if(message.channel.type === "dm")
        return
    if(msg[0] == settings.prefix) {
        var args = msg.split(" ")
        if(args[0] == "!filter" && args.length == 2) {
          setTimeout(refreshFilter, 3000);
          return
        }
    }
	var i = 0;
    for(word of filter["words"]) {
		i++;
        if(msg.includes(word)) {
            let role = message.guild.roles.find(r => r.name == "Muted")
            message.delete()
            await message.guild.member(message.author.id).addRole(role).then(mute(message, message,"WordF:#"+i))
            return
        }
    }
})

bot.on("guildMemberAdd", (member) => {
    member.guild.defaultChannel.send(`Welcome to ${member.guild.name}'s discord, ${member.user}. Let me know if you need any help.`)
})

bot.registry
    .registerGroups([
        ['about','Server info commands'],
        ['fun', 'Fun commands'],
        ['music', 'Music commands'],
        ['mod', 'Moderation commands'],
        ['util','Utility commands']
    ])
    .registerDefaults()
    .registerCommandsIn(__dirname+'/commands');

bot.login(settings.token)

process.stdin.resume();

function exitHandler(options, err) {
    if (options.cleanup) console.log('clean');
    if (err) console.log(err.stack);
    if (options.exit) process.exit();
    console.log("Shut down...")
}

process.on('exit', exitHandler.bind(null,{cleanup:true}));

process.on('SIGINT', exitHandler.bind(null, {exit:true}));

process.on('uncaughtException', exitHandler.bind(null, {exit:true}));
