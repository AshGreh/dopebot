const commando = require("discord.js-commando")
const request = require("request-promise")
const { oneLine, stripIndents } = require('common-tags')
const { version } = require('../../package')

class Poll extends commando.Command {
    constructor(client) {
        super(client, {
            name:'poll',
            group:'mod',
            memberName:'poll',
            description:'Creates a strawpoll',
            details:stripIndents`Create a strawpoll.
				            The first argument is always the title, if you provde it, otherwise your username will be used!
				            If you need to use spaces in your title make sure you put them in SingleQuotes => \`'topic here'\``,
            guildOnly:true,
            throttling:{
              usages:1,
              duration:60
            },
            args:[
              {
                key:'title',
                prompt:'What is the title of the poll?',
                type:'string',
                validate:title=>{
                  if(title.length<1||title.length>200) {
                    return `Your title had ${title.length} characters. The count of characters should be between 1 and 200.`
                  }
                  return true;
                }
              },
              {
                key:'options',
                prompt:'What options would you like to have?(Each line is an option.)',
                type:'string',
                validate:options=>{
                  if(options.length<1||options.length>160) {
                    return 'Your option was ${option.length} characters. The count of characters should be between 1 and 60.'
                  }
                  return true;
                },
                infinite: true
              }
            ],
            usage:'!poll <The title> <The options>*',
            examples: ['!pin Title']
        });
    }

    async run(message, { title, options }) {
      if(!message.member.hasPermission("MANAGE_MESSAGES")) {
          return
      }
      if(options.length<2||options.length>32) {
          return message.reply("Options length is not in range.")
      }
      const response = await request({
        method:'POST',
        uri:'https://strawpoll.me/api/v2/polls',
        followAllRedirects:true,
        headers:{'User-Agent':`DAFB v${version}`},
        body:{
          title:title,
          options:options
        },
        json:true
      })
      return message.say(`${response.title}
			http://strawpoll.me/${response.id}`)
    }
}

module.exports = Poll;
