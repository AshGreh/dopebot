const commando = require('discord.js-commando');

class DiceRoll extends commando.Command {
    
    constructor(client) {
        super(client, {
            name:'roll',
            group:'fun',
            memberName:'roll',
            description:'Rolls a die',
            usage:'!roll',
            examples: ['!roll']
        });
    }

    async run(message, args) {
        var roll = Math.floor(Math.random() * 100)+1;
        message.reply("You rolled a "+roll);
    }

}

module.exports = DiceRoll;