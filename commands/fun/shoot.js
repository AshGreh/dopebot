const commando = require("discord.js-commando")

class Shoot extends commando.Command {
    constructor(client) {
        super(client, {
            name:'shoot',
            group:'fun',
            memberName:'shoot',
            usage:'!shoot @<Name of the person>',
            description:'Shoots someone',
            examples: ['!shoot someone']
        })
    }

    async run(message, args) {
        var roll = Math.floor(Math.random() * 2)+1;
        if(roll == 1)
            if(args!='')
                message.channel.send(message.author+" shot "+args+"!")
            else
                message.channel.send(message.author+" shoots himself.")
        else
            if(args!='')
                message.channel.send(args+" shot"+message.author+"!")
            else
                message.channel.send("Someone shot "+message.author+" down!")
    }
}

module.exports = Shoot;
