const commando = require("discord.js-commando")
const json = require("jsonfile")

class Filter extends commando.Command {
    constructor(client) {
        super(client, {
            name:'filter',
            group:'mod',
            memberName:'filter',
            description:'Adds  to the filter of words',
            usage:'!filter <word to filter>',
            examples: ['!filter alva'],
            args: [{
                key: 'word',
                prompt: 'Which word do you want to filter?',
                type: 'string'
            }],
            guildOnly: true
        });
    }

    async run(message, args) {
      if(!message.member.hasPermission("MANAGE_MESSAGES")) {
          return
      }
      console.log(args);
      var filter = json.readFileSync('./ignore/filter.json')
      var wordToFilter = args["word"];
      if(filter["words"].includes(wordToFilter)) {
        message.reply("Removed word:"+wordToFilter)
        filter["words"] = filter["words"].filter(word => {
            return word != wordToFilter
        })
      } else {
        filter["words"].push(wordToFilter)
        message.reply("word has been added")
      }
      message.reply("current filter includes "+filter["words"])
      json.writeFileSync('./ignore/filter.json', filter)
    }
}

module.exports = Filter;
