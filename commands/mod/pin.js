const commando = require("discord.js-commando")

class Pin extends commando.Command {
    constructor(client) {
        super(client, {
            name:'pin',
            group:'mod',
            memberName:'pin',
            description:'Pins the comment',
            usage:'!pin <Comment to pin>',
            guildOnly: true,
            examples: ['!pin This comment.']
        });
    }

    async run(message, args) {
        if(!message.member.hasPermission("MANAGE_MESSAGES")) {
            message.channel.send(args).then(msg=>msg.pin());
            message.delete().then().catch(console.error);
        }
    }
}

module.exports = Pin;
