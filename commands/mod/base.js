const commando = require("discord.js-commando")

class Base extends commando.Command {
    constructor(client) {
        super(client, {
            name:'pin',
            group:'mod',
            memberName:'pin',
            description:'Pins the comment',
            usage:'!pin <The comment to pin>',
            examples: ['!pin This comment.']
        });
    }

    async run(message, args) {
      if(!message.member.hasPermission("MANAGE_MESSAGES")) {
          return
      }
    }
}

//module.exports = Base;
