const commando = require("discord.js-commando")

class Wipe extends commando.Command {
    constructor(client) {
        super(client, {
            name:'wipe',
            aliases:['nuke','purge','clear','clean','prune'],
            group:'mod',
            memberName:'wipe',
            details: `Deletes messages. Here is a list of filters:
				    __invites:__ Messages containing an invite
				    __user @user:__ Messages sent by @user
				    __bots:__ Messages sent by bots
				    __uploads:__ Messages containing an attachment
				    __links:__ Messages containing a link`,
            description:'Deletes the specified number of comment[Broken]',
            usage:'!wipe <Number of messages to delete> <filter> @<member>',
            examples: ['!wipe 100', '!wipe 5 user @DopeAssFreshPrince', '!nuke links'],
            guildOnly: true,
            args: [
                      {
					              key: 'limit',
					              prompt: 'how many messages would you like to delete?\n',
					              type: 'integer',
					              max: 100
				              },
				              {
					              key: 'filter',
              					prompt: 'what filter would you like to apply?\n',
					              type: 'string',
					              default: '',
					              parse: str => str.toLowerCase()
				               },
				               {
					               key: 'member',
					               prompt: 'whose messages would you like to delete?\n',
					               type: 'member',
					               default: ''
				               }
			    ]
        });
    }

    async run(message, { filter, limit, member }) {
        if(!message.member.roles.exists('name','Super Moderators')) {
            return
        }
        let messageFilter
        if(filter) {
          if(filter == 'invite') {
				          messageFilter = message => message.content.search(/(discord\.gg\/.+|discordapp\.com\/invite\/.+)/i) !== -1;
			      }else if(filter === 'user') {
				         if(member) {
					           const user = member.user;
					           messageFilter = message => message.author.id === user.id;
				         }else{
					           return message.say(`${message.author}, you have to mention someone.`);
				         }
			      } else if (filter === 'bots') {
				         messageFilter = message => message.author.bot;
			      } else if (filter === 'you') {
				         messageFilter = message => message.author.id === this.client.user.id;
			      } else if (filter === 'upload') {
				         messageFilter = message => message.attachments.size !== 0;
			      } else if (filter === 'links') {
				         messageFilter = message => message.content.search(/https?:\/\/[^ \/\.]+\.[^ \/\.]+/) !== -1; // eslint-disable-line no-useless-escape, max-len
			      } else {
				         return msg.say(`${message.author}, this is not a valid filter. \`help clean\` for all available filters.`);
			      }
            const messages = await message.channel.fetchMessages({ limit: limit }).catch(err => null);
            const messagesToDelete = messages.filter(messageFilter)
            message.channel.bulkDelete(messagesToDelete.array().reverse()).catch(err => null)
          } else {
            const messagesToDelete = await message.channel.fetchMessages({ limit: limit }).catch(err => null)
            message.channel.bulkDelete(messagesToDelete.array().reverse()).catch(err => null);
          }
          message.reply(" your request has been executed.")
    }
}

module.exports = Wipe;
