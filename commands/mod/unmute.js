const commando = require("discord.js-commando")

class Base extends commando.Command {
    constructor(client) {
        super(client, {
            name:'unmute',
            group:'mod',
            memberName:'unmute',
            description:'Disables mute',
            usage:'!wipe @<Person to unmute>',
            guildOnly: true,
            examples: ['!unmute @DopeAssFreshPrince']
        });
    }

    async run(message, args) {
        if(!message.member.hasPermission("MANAGE_MESSAGES")) {
          return
        }
        let toMute = message.guild.member(message.mentions.users.first())
        if(!toMute)
            return message.channel.sendMessage("Incorrect user, please check your mention.");
        let role = message.guild.roles.find(r => r.name == "Muted");
        if(!role) {
            try {
            role = await message.guild.createRole({
                name: "Muted",
                color: "#000",
                permissions: []
            })

            message.guild.channels.forEach(async (channel, id) => {
                await channel.overwritePermissions(role, {
                    SEND_MESSAGES: false,
                    ADD_REACTIONS: false
                })
            })
            } catch(e) {
             message.reply("Bot failed. CODE:0000x2")
             console.log(e.stack)
            return
            }
        }
        if(!toMute.roles.has(role.id)) return message.reply(toMute+" is not muted!");
        await toMute.removeRole(role);
        message.channel.send(toMute+"'s mute has been removed. Please refresh your memory on the rules found here: https://www.powerbot.org/community/topic/1315351-house-rules/.")
        await message.delete();
    }
}

module.exports = Base;
