const commando = require("discord.js-commando")

class Mute extends commando.Command {
    constructor(client) {
        super(client, {
            name:'mute',
            group:'mod',
            memberName:'mute',
            description:'Mutes the requested person',
            usage:'!mute @<Username> <Reason that can have spaces>',
            examples: ['!mute @DopeAssFreshPrince being too awesome'],
            guildOnly: true,
            args: [
                {
                    key: 'user',
                    prompt: 'Which user do you want to mute?',
                    type: 'user'
                },
                {
                    key: 'reason',
                    prompt: 'What is the reason behind their mute?',
                    type: 'string'
                }
            ]
        });
    }

    async run(message, args) {
        if(!message.member.hasPermission("MANAGE_MESSAGES")) {
            return
        }
        let toMute = message.guild.member(message.mentions.users.first())
        if(!toMute)
            return message.channel.sendMessage("Incorrect user, please check your mention.")
        let role = message.guild.roles.find(r => r.name == "Muted")
        if(!role) {
            try {
            role = await message.guild.createRole({
                name: "Muted",
                color: "#000",
                permissions: []
            })

            message.guild.channels.forEach(async (channel, id) => {
                await channel.overwritePermissions(role, {
                    SEND_MESSAGES: false,
                    ADD_REACTIONS: false
                })
            })
            } catch(e) {
             message.reply("Bot failed. CODE:0000x1")
             console.log(e.stack)
            return
            }
        }
        if(toMute.roles.has(role.id)) return message.reply(toMute+" has already been muted!");
        await toMute.addRole(role);
        if(args['reason'] == '') {
            args['reason'] = 'no reason'
        }
        message.channel.send(toMute+" has been muted for "+args['reason']+". You may appeal the mute at https://www.powerbot.org/community/forum/381-support/. ")
        toMute.sendMessage("You have been muted for "+args['reason']+" by "+message.author+". You may appeal the mute at https://www.powerbot.org/community/forum/381-support/. ")
        await message.delete();
    }
}

module.exports = Mute;
