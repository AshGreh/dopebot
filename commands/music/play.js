const commando = require("discord.js-commando")
const youtube = require("ytdl-core")

class Play extends commando.Command {
    constructor(client) {
        super(client, {
            name:'play',
            group:'mod',
            memberName:'play',
            description:'To queue a song',
            usage:'!play <The youtube song to play>',
            examples: ['!play nyan cat 10 hours']
        });
        this.queue = new Map();
        this.youtube = new youtube("")
    }

    async run(message, args) {

    }
}

//module.exports = Play;
