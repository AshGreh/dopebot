const commando = require("discord.js-commando")
const moment = require('moment');
const { stripIndents } = require('common-tags');

const verificationLevel = {
	0: 'None',
	1: 'Low',
	2: 'Medium',
	3: '(╯°□°）╯︵ ┻━┻',
	4: '┻━┻ ﾐヽ(ಠ益ಠ)ノ彡┻━┻'
};

class UserInfo extends commando.Command {
    constructor(client) {
        super(client, {
            name:'user',
            group:'about',
            memberName:'user',
            description:'Gives information about the specified user',
            usage:'!user @<Username>',
            examples: ['!user'],
						guildOnly: true,
						args:[
							{
								key:'member',
								prompt:'What is the name of the user?',
								type:'member'
							}
						]
        });
    }

    async run(message, args) {
      if(!message.member.hasPermission("MANAGE_MESSAGES")) {
          return
      }
			const member = args.member
			const user = member.user
			//const usernames = await username.findAll({where:{userID:user.id}})
      return message.embed({
			color: 663399,
			description: `${member.displayName} (ID: ${member.id})`,
			fields: [
				{
					name: '❯ Member Details',
					value: stripIndents`
						${member.nickname !== null ? ` • Nickname: ${member.nickname}` : '• No nickname'}
						• Roles: ${member.roles.map(roles => `\`${roles.name}\``).join(' ')}
						• Joined at: ${moment.utc(member.joinedAt).format('dddd, MMMM Do YYYY, HH:mm:ss ZZ')}
					`
				},
				{
					name: '❯ User Details',
					/* eslint-disable max-len */
					value: stripIndents`
						• Created at: ${moment.utc(user.createdAt).format('dddd, MMMM Do YYYY, HH:mm:ss ZZ')}${user.bot ? '\n• Is a bot account' : ''}
						• Status: ${user.presence.status}
						• Game: ${user.presence.game ? user.presence.game.name : 'None'}
					`
					/* eslint-enable max-len */
				}
			],
			thumbnail: { url: user.displayAvatarURL }
		});
    }
}

module.exports = UserInfo;
