const commando = require("discord.js-commando")
const moment = require('moment');
const { stripIndents } = require('common-tags');

const verificationLevel = {
	0: 'None',
	1: 'Low',
	2: 'Medium',
	3: '(╯°□°）╯︵ ┻━┻',
	4: '┻━┻ ﾐヽ(ಠ益ಠ)ノ彡┻━┻'
};

class ServerInfo extends commando.Command {
    constructor(client) {
        super(client, {
            name:'server',
            group:'about',
            memberName:'server',
            description:'Gives information about the server',
            usage:'!server',
            examples: ['!server'],
						guildOnly: true
        });
    }

    async run(message, args) {
      if(!message.member.hasPermission("MANAGE_MESSAGES")) {
          return
      }
      return message.embed({
			color: 663399,
			description: `${message.guild.name} (ID: ${message.guild.id})`,
			fields: [
				{
					name: '❯ Channels',
					/* eslint-disable max-len */
					value: stripIndents`
						• ${message.guild.channels.filter(ch => ch.type === 'text').size} Text, ${message.guild.channels.filter(ch => ch.type === 'voice').size} Voice
						• Default: ${message.guild.defaultChannel}
						• AFK: ${message.guild.afkChannelID ? `<#${message.guild.afkChannelID}> after ${message.guild.afkTimeout / 60}min` : 'None.'}
					`,
					/* eslint-enable max-len */
					inline: true
				},
				{
					name: '❯ Member',
					value: stripIndents`
						• ${message.guild.memberCount} members
						• Owner: ${message.guild.owner.user.username}#${message.guild.owner.user.discriminator}
						(ID: ${message.guild.ownerID})
					`,
					inline: true
				},
				{
					name: '❯ Other',
					value: stripIndents`
						• Roles: ${message.guild.roles.size}
						• Region: ${message.guild.region}
						• Created at: ${moment.utc(message.guild.createdAt).format('dddd, MMMM Do YYYY, HH:mm:ss ZZ')}
						• Verification Level: ${verificationLevel[message.guild.verificationLevel]}
					`
				}
			],
			thumbnail: { url: message.guild.iconURL }
		});
    }
}

module.exports = ServerInfo;
