const commando = require("discord.js-commando")

class Base extends commando.Command {
    constructor(client) {
        super(client, {
            name:'audit',
            group:'about',
            memberName:'audit',
            description:'Shows the audit log of a user.',
            usage:'!audit @<User to audit>',
            examples: ['!audit @DopeAssFreshPrince']
        });
    }

    async run(message, args) {
      if(!message.member.hasPermission("MANAGE_MESSAGES")) {
          return
      }
    }
}

//module.exports = Base;
