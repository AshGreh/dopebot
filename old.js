const commando = require('discord.js-commando');
const settings = require('./settings.json');

const bot = new commando.Client({
    owner: settings.owner,
    commandPrefix: settings.prefix
})

bot.registry
    .registerGroups([
        ['fun', 'Fun commands'],
        ['music', 'Music commands'],
        ['mod', 'Moderation commands']
    ])
    .registerDefaults()
    .registerCommandsIn(__dirname+'/commands');

bot.login(settings.token)
